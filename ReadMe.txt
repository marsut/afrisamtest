Git best practices

*  Production branch/main branch is most stable code
*  Work on/from preproduction branch
*  Pull regularly & always before you start coding
*  Commit frequently with comments describing "why"
*  Add your name to commits comments


Branch naming strategies

*What does the branch do?
bugfix
new feature

* Everyone push and pull to and from preproduction branch
* Create all feature branches from preproduction branch
* For small changes - work directly in preproduction branch
* New features - create a feature branch

Always pull before pushing and merge conflicts

Basic coding practices

* Use comments that explain what the code does
* Use descriptive names for variables/functions
* Be consistent in layout/indentation
* Use consistent naming scheme - camelCase or underscores
* Avoid deep nesting
* Limit line length
* Capitalize SQL special words
* Do not hardcode
* Don't reinvent the wheel
* Test & do peer reviews/pull requests


